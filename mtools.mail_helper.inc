<?php

/**
 * Implements hook_mail().
 */
function mtools_mail($key, &$message, $params) {
  $headers = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/plain',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer' => 'Drupal',
  );
  foreach ($headers as $k => $v) {
    $message['headers'][$k] = $v;
  }
  $message['subject'] = t($params['subject']);
  $message['body'][] = $params['body'];
}