<?php

define('INDEX_TOO_LONG', 1);
define('INDEX_ALREADY_EXISTS', 2);
define('INDEX_ILLEGAL_TYPE', 3);

/**
 * Implements hook_drush_command().
 */
function mtools_drush_command() {
  // The key in the $items array is the name of the command.
  $items = [];
  $items['mt-clear-vocabulary'] = [
    'description' => "Remove all terms for a particular vocabulary.",
    'drupal dependencies' => ['taxonomy'],
    'arguments' => [
      'vocabulary' => 'The vocabulary machine name to clear.',
    ],
    'examples' => [
      'mt-clear-vocabulary example_vocabulary' => 'Remove all terms from the "example_vocabulary" vocabulary.',
    ],
    'aliases' => ['mtcv'],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];
  $items['mt-delete-all-nodes'] = [
    'description' => "Delete all content in the Drupal database. Delete *all* nodes.",
    'aliases' => ['mtdan'],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];
  $items['mt-delete-node-type'] = [
    'description' => "Delete all the nodes of a specific type in the Drupal database.",
    'arguments' => [
      'type' => 'The node type to clear.',
    ],
    'aliases' => ['mtdnt'],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];
  $items['mt-delete-all-users'] = [
    'description' => 'Delete all users except for uid 1',
    'aliases' => ['mtdau'],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];
  $items['mt-delete-unused-accounts'] = array(
    'description' => 'Deletes account that have no content or comments associated with them or that have not been used or verified.',
    'aliases' => ['mtdua'],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  $items['mt-add-indexes'] = [
    'description' => 'Adds indexes to all custom content fields.',
    'aliases' => ['mtai'],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];
  $items['mt-search-replace'] = [
    'description' => t('Does a search and replace on content across all nodes. Must be configured in admin panel first.'),
    'bootstrp' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => [
      'search' => 'The string to be searched for.',
      'repalce' => 'The string to replace the string seached for.',
    ],
    'aliases' => ['mtsr'],
    'examples' => [
      'mt-search-replace "sites/default/files" "sites/domain/files' => 'Change a path from the default path to public files to one that is domain specific (good for multisite conversions).',
    ],
  ];
  $items['mt-file-scan'] = [
    'description' => t('Scans public file directories for files of a specific type.'),
    'bootstrp' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => [
      'search' => 'The file extension to be searched for.',
      'xref' => 'Cross reference with managed files and add to csv.',
    ],
    'aliases' => ['mtfs'],
    'examples' => [
      'mfs pdf' => 'Search for files that have pdf in them (make .pdf for extensions).',
      'mfs' => 'Search for everything',
      'mfs * 1' => 'Search all files and cross reference with managed files.',
    ],
  ];
  return $items;
}

/**
 * Impements drush_HOOK_COMMAND().
 * Look for files in our file system and report on the based on arguments.
 * 
 * @param string search
 *   The pattern to be searched for. This should be set to * for all if you
 *   are also wanting to search with cross references.
 * 
 * @param bool xref
 *   Set this to 1 if you would like all files to be cross references against
 *   managed files.
 */
function drush_mtools_mt_file_scan($search = NULL, $xref = FALSE) {
  $entries = [];
  if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
    $public_file_path = $wrapper->realpath();
    if ($search != NULL && $search != "*") {
      $search = '/.*\\'. $search. '$/';
      $files = file_scan_directory($public_file_path, $search);
    }
    else {
      $files = file_scan_directory($files);
    }
  }
  else {
    drush_log('Could not determine location of public file system.', 'error');
  }

  foreach ($files as $file) {
    $length = strlen($public_file_path);
    if ($public_file_path == substr($file->uri, 0, $length)) {
      $uri = 'public:/' . substr($file->uri, $length);
    }

    $query = db_select('file_managed', 'fm')
      ->fields('fm', ['filename', 'uri', 'filesize', 'status'])
      ->fields('fu', ['count', 'type', 'id'])
      ->condition('fm.uri', $uri);
    $query->leftJoin('file_usage', 'fu', 'fu.fid=fm.fid');
    $result = $query->execute();

    $find = [];
    while ($record = $result->fetchObject()) {
      $find[] = $record;
    }
    if (count($find) > 0) {
      foreach ($find as $record) {
        if ($record->type == 'node') {
          $q2 = db_select('node', 'n')
            ->fields('n')
            ->condition('n.nid', $record->id)
            ->execute();
          $r2 = $q2->fetchObject();
          $record->title = $r2->title;
        }
        else {
          $record->title = 'Not a node';
        }

        $entry = [
          'uri' => $uri,
          'filename' => $record->filename,
          'filesize' => $record->filesize,
          'status' => $record->status,
          'title' => $record->title,
          'type' => $record->type,
          'type_id' => $record->id,
        ];
        $entries[] = str_putcsv($entry);
      }
    }
  }

  $headers = ['uri', 'filename', 'filesize', 'status', 'title', 'type', 'type_id'];
  $headers = str_putcsv($headers);

  print $headers . "\n";
  foreach ($entries as $entry) {
    print $entry . "\n";
  }
}

/**
 * Implements drush_HOOK_COMMAND()
 * Go through the nodes and search and replace all fields with the search criteria
 * specified. Does this on a field by field basis across data and revision tables.
 */
function drush_mtools_mt_search_replace($search = NULL, $replace = NULL) {
  if (empty($search) || empty($replace)) {
    drush_set_error('No search and replace criteria specified.');
  }
  else {
    $result = db_select('field_config', 'f')
      ->fields('f')
      ->execute();
    while($data = $result->fetchObject()) {
      $data_name = 'field_data_' . $data->field_name;
      $revision_name = 'field_revision_' . $data->field_name;
      $value_name = $data->field_name . "_value";

      try {
        $field = db_select($data_name, 'd')
          ->fields('d', [$value_name, 'entity_id'])
          ->condition('d.entity_id', '1', '>=')
          ->condition('d.' . $value_name, '%' . db_like($search) . '%', 'LIKE')
          ->execute();
      }
      catch (PDOException $e) {
        continue;
      }
      while($fv = $field->fetchObject()) {
        if (@unserialize($fv->$value_name) === FALSE || !empty($fv->$value_name)) {
          $new_value = str_replace($search, $replace, $fv->$value_name);
          db_update($data_name)
            ->fields([
              $value_name => $new_value,
            ])
            ->condition('entity_id', $fv->entity_id, '=')
            ->execute();
        }
      }

      try {
        $field = db_select($revision_name, 'd')
          ->fields('d', [$value_name, 'revision_id', 'entity_id'])
          ->condition('d.' . $value_name, '%' . db_like($search) . '%', 'LIKE')
          ->condition('d.revision_id', '1', '>=')
          ->execute();
      }
      catch (PDOException $e) {
        continue;
      }
      while($fv = $field->fetchObject()) {
        if (@unserialize($fv->$value_name) === FALSE || !empty($fv->$value_name)) {
          $new_value = str_replace($search, $replace, $fv->$value_name);
          db_update($revision_name)
            ->fields([
              $value_name => $new_value,
            ])
            ->condition('revision_id', $fv->revision_id, '=')
            ->execute();
        }
      }
    }
  }
}

/**
 * Implements drush_HOOK_COMMAND()
 * Handler to delete all users with a uid greater than 1. The first user is a special
 * Drupal case and we never want to delete that.
 */
function drush_mtools_mt_delete_all_users() {
  // Get the complete list of uid's > 1
  $uids = [];
  $result = db_query("SELECT uid FROM {users} WHERE uid > 1");
  while($udata = $result->fetchObject()) {
    $uids[] = $udata->uid;
  }
  drush_log('Removing ' . count($uids) . ' users.', 'ok');
  user_delete_multiple($uids);
  drush_log(count($uids) . ' users removed.' ,'ok');
}

/**
 * Implements drush_HOOK_COMMAND()
 * Handler to delete all users with a uid greater than 1. The first user is a special
 * Drupal case and we never want to delete that.
 */
function drush_mtools_mt_delete_unused_accounts() {
  // Get the complete list of uid's > 1 that have no nodes or comments assigned to them
  // Merge with a list that have  not be accessed, verified, or logged in.
  $uids = [];
  $result = db_query("SELECT u.uid AS user_user_id,
                             n.uid AS node_user_id, 
                             c.uid AS comment_user_id
                      FROM {users} u
                      LEFT JOIN {node} n ON n.uid=u.uid
                      LEFT JOIN {comment} c ON c.uid=u.uid
                      WHERE n.uid IS NULL 
                        AND c.uid IS NULL
                        AND u.uid > 1");
  while($udata = $result->fetchObject()) {
    $uids[$udata->user_user_id] = $udata->user_user_id;
  }
  $result = db_query("SELECT u.uid AS user_user_id
                      FROM {users} u
                      WHERE (u.access=:access OR u.login=:login OR u.status=:status)",
                      [':access' => 0, ':login' => 0, ':status' => 0]);
  while($udata = $result->fetchObject()) {
    $uids[$udata->user_user_id] = $udata->user_user_id;
  }
  drush_log('Removing ' . count($uids) . ' users.', 'ok');
  user_delete_multiple($uids);
  drush_log(count($uids) . ' users removed.' ,'ok');
}

/**
 * Implements drush_HOOK_COMMAND()
 *
 * Clear everything from our nodes table relative to the content type
 */
function drush_mtools_mt_delete_node_type($type = NULL) {
  // If a node type was not provided, then give a list of node types for the user to
  // select from
  if (empty($type)) {
    $types = node_type_get_types();
    $options = ['all' => 'All'];
    foreach ($types as $type=>$object) {
      $options[$type] = $object->name;
    }
    $choice = drush_choice($options, 'Which node type do you wish to clear?'); 
    if ($choice == 'all') {
      foreach($options as $key => $option) {
        if ($key == 'all') {
          continue;
        }
        mtools_delete_all_content();
      }
    } 
    else {
      mtools_detete_select_content([$choice => $choice], TRUE);
    }
  }
}    

/**
 * Implements drush_HOOK_COMMAND()
 * Clear everything from our nodes table regardless of content type. 
 */
function drush_mtools_mt_delete_all_nodes() {
  mtools_delete_all_content();
  /* we're going to list the deleted nodes by title */
  $result = db_query("SELECT nid, title FROM {node} ORDER BY nid");

  /* get the number of nodes */
  $node_count = $result->rowCount();
  drush_log("Deleting " . $node_count . " nodes", 'ok');
  mtools_delete_all_content();
}

/**
 * Implements drush_HOOK_COMMAND()
 * Remove all terms from a specific taxonomy vocabulary
 */
function drush_mtools_mt_clear_vocabulary($vocabulary = FALSE) {
  // If the vocabulary was not specified, give them a list to choose from, the first on
  // the list being "all"
  if (empty($vocabulary) || $vocabulary == FALSE) {
    $options = ['all' => 'All'];
    $vocabularies = taxonomy_get_vocabularies();
    foreach($vocabularies as $vocabulary) {
      $options[$vocabulary->machine_name] = $vocabulary->name;
    }
    $choice = drush_choice($options, 'Which vocabulary do you wish to clear?'); 
    if ($choice && array_key_exists($choice, $options)) {
      if ($choice == 'all') {
        foreach($options as $key => $option) {
          if ($key == 'all') {
            continue;
          }
          $vocabulary_object = taxonomy_vocabulary_machine_name_load($key);
          _mtools_delete_vocabulary($vocabulary_object);
        }
      } 
      else {
        $vocabulary_object = taxonomy_vocabulary_machine_name_load($choice);
        _mtools_delete_vocabulary($vocabulary_object);
      } 
    }
    else {
      drush_set_error('Unable to locate any vocabulary named ' . $choice);
    }
  }
  else {
    if ($vocabulary_object = taxonomy_vocabulary_machine_name_load($vocabulary)) {
      _mtools_delete_vocabulary($vocabulary_object);
    } 
    else {
      drush_set_error('Unable to locate any vocabulary named ' . $vocabulary);
    }
  }
}

/**
 * Adds indexes to additional database fields for fields added to entity types.
 * This is useful if you are querying off non-key values in a large nodeset, but has
 * the potential to explode the size of your database on disk (indexes).
 */
function drush_mtools_mt_add_indexes() {
  $message = [
    '',
    'Unable to index field, length of index would be too long.',
    'Unable to index field, index already exists.',
    'Unable to index field, not a valid field for indexing.',
  ];
  $fields = array_keys(field_info_fields());
  $field_data_prefix = 'field_data_';
  foreach ($fields as $field) {
    $table = $field_data_prefix . $field;
    $data = db_query("SHOW COLUMNS FROM $table", array());
    while ($column = $data->fetchAssoc()) {
      // Initialize our condition
      $index_condition = NULL;
      // Check to see if the amount of characters we're trying to index is > 255 for varchar      
      $matches = [];
      if (stristr($column['Type'], 'varchar')) {
        preg_match_all('/\((.*?)\)/', $column['Type'], $matches);
        $length = $matches[1][0];
        if ($length > 255) {
          $index_condition = INDEX_TOO_LONG;
        }
      }
      if ($index_condition == NULL && !empty($column['Key'])) {
        $index_condition = INDEX_ALREADY_EXISTS;
      }
      if (stristr($column['Type'], 'text') || stristr($column['Type'], 'blob') || stristr($column['Type'], 'longblob') || stristr($column['Type'], 'longtext')) {
        $index_condition = INDEX_ILLEGAL_TYPE;
      }
      if (stristr($column['Field'], 'data') || stristr($column['Field'], 'format') || stristr($column['Field'], 'entity_type')) {
        $index_condition = INDEX_ILLEGAL_TYPE;
      }
      if ($index_condition == NULL) {
        db_add_index($table, $column['Field'], array($column['Field']));
        drush_log($column['Field'] , 'success');
      }
      else {
        if ($index_condition != INDEX_ALREADY_EXISTS) {
          // I really don't want a ton of output on this especially for a "run once" type
          // of script that would throw a ton of messages on subsequent runs.
          // drush_log($column['Field'] . ': ' . $message[$index_condition], 'error');
        }   
      }
    }
  }
}

/**
 * Helper function to delete a vocabulary
 *
 * @param object $vocabulary
 *   The Drupal vocabulary object to be deleted
 */
function _mtools_delete_vocabulary($vocabulary) {
  $tree = taxonomy_get_tree($vocabulary->vid);
  foreach($tree as $term) {
    if (taxonomy_term_delete($term->tid) == SAVED_DELETED) {
      drush_log($term->name . ' successfully deleted.', 'ok');
    } else {
      drush_log($term->name . ' could not be deleted.', 'error');
    }
  }
}

if (!function_exists('str_putcsv')) {
  function str_putcsv($input, $delimiter = ',', $enclosure = '"') {
    // Open a memory "file" for read/write...
    $fp = fopen('php://temp', 'r+');
    // ... write the $input array to the "file" using fputcsv()...
    fputcsv($fp, $input, $delimiter, $enclosure);
    // ... rewind the "file" so we can read what we just wrote...
    rewind($fp);
    // ... read the entire line into a variable...
    $data = fgets($fp);
    // ... close the "file"...
    fclose($fp);
    // ... and return the $data to the caller, with the trailing newline from fgets() removed.
    return rtrim( $data, "\n" );
  }
}