<?php

/**
 * Handle Submitted Files
 */ 
 function mtools_managed_file_submit($form, $form_state, $managed_files = NULL, $module = NULL, $type = NULL, $id = NULL) {  
  // Convert managed files to an array if we were passed a scalar.
  if (!is_array($managed_files)) {
    $managed_files = array($managed_files);
  }
  // Iterate through the managed files list provided by the caller
  foreach ($managed_files as $managed_file) {
    // Get the original file if one exists
    $existing_file = (!empty($form[$managed_file]['#default_value'])) ? $form[$managed_file]['#default_value'] : 0;
    /* if we have been provided files, make sure to mark them as in use */
    if (isset($form_state['values'][$managed_file]) && $form_state['values'][$managed_file]) {
      // If the uploaded file is different than the one we already have, then we need to
      // Remove the current file and replace it with the new one
      if (isset($existing_file) && $existing_file != $form_state['values'][$managed_file]) {
        _mtools_managed_file_remove_managed_file($existing_file, $module, $type, $id);
        _mtools_managed_file_add_managed_file($form_state['values'][$managed_file], $module, $type, $id);
      }
      else {
        _mtools_managed_file_add_managed_file($form_state['values'][$managed_file], $module, $type, $id);
      }
    }
    elseif (!empty($existing_file)) {
      _mtools_managed_file_remove_managed_file($existing_file, $module, $type, $id);
    }
  }
}

function _mtools_managed_file_remove_managed_file($managed_file, $module, $type = 'file', $id = NULL) {  
  // Retrieve the old file's id.
  $file = (!empty($managed_file)) ? file_load($managed_file) : FALSE;
  if ($file) {
    // If we were not passed an id, then use the file id
    $id = (empty($id)) ? $file->fid : $id;
    // When a module is managing a file, it must manage the usage count.
    // Here we decrement the usage count with file_usage_delete().
    file_usage_delete($file, $module, $type, $id);
    // The file_delete() function takes a file object and checks to see if
    // the file is being used by any other modules. If it is the delete
    // operation is cancelled, otherwise the file is deleted.
    file_delete($file);
  }
}

function _mtools_managed_file_add_managed_file($managed_file, $module, $type = 'file', $id = NULL) {
  // If our file is already in use, then we don't need to re-do this and increase the count
  $count = db_query('SELECT COUNT(*) FROM {file_usage} WHERE fid=:fid', array('fid' => $managed_file))->fetchField();
  if (empty($count)) {
    // Load the file via fid
    $file = file_load($managed_file);
    // If we were not passed an id, then use the file id
    $id = (empty($id)) ? $file->fid : $id;
    // change status to permanent
    $file->status = FILE_STATUS_PERMANENT;
    // Save the file
    file_save($file);
    // Record the file as in use
    file_usage_add($file, $module, $type, $id);
    unset($file);
  }
}