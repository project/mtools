jQuery(document).ready(function($) {
    $('form').bind('state:visible', function(e) {
        if(e.trigger) {
            $(e.target).closest('.form-item, .form-submit, .form-wrapper')[e.value ? 'slideDown' : 'slideUp']();
            e.stopPropagation();
        }
    });
});